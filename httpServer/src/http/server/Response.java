package http.server;

import java.io.IOException;
import java.io.OutputStream;
 

public class Response {

	Request request;
	OutputStream output;
	
	public Response(OutputStream output){
		this.output=output;
	}
	
	public void setRequest(Request request){
		this.request=request;
	}
	
	public void sendResponse() throws IOException{
		try {
			String uri = request.getUri();
//			System.out.println(uri);
			String[]uris = uri.split("\\?");
			if(uris!=null && uris.length>0 && uris[0].equals("/add")) {
				String[] params = uris[1].split("&");
				int a = Integer.parseInt(params[0].substring(params[0].length()-1));
				int b = Integer.parseInt(params[1].substring(params[1].length()-1));
				String message="HTTP/1.1 404 File Not Found\r\n"+
						"Content-Type:text/html\r\n"+
						"Content-Length:23\r\n"+
						"\r\n"+
						"<h1>"+(a+b)+"</h1>";
						output.write(message.getBytes());
			}else if(uris!=null && uris.length>0 && uris[0].equals("/mult")) {
				String[] params = uris[1].split("&");
				int a = Integer.parseInt(params[0].substring(params[0].length()-1));
				int b = Integer.parseInt(params[1].substring(params[1].length()-1));
				String message="HTTP/1.1 404 File Not Found\r\n"+
						"Content-Type:text/html\r\n"+
						"Content-Length:23\r\n"+
						"\r\n"+
						"<h1>"+(a*b)+"</h1>";
						output.write(message.getBytes());
			}else {
				String errorMessage="HTTP/1.1 404 File Not Found\r\n"+
						"Content-Type:text/html\r\n"+
						"Content-Length:23\r\n"+
						"\r\n"+
						"<h1>404</h1>";
						output.write(errorMessage.getBytes());
			}
				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

